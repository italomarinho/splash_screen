import 'package:flutter/material.dart';

import 'package:flutter_modular/flutter_modular.dart';

import 'app/app_module.dart';
import 'app/app_widget.dart';
import 'app/core/pages/splash_screen.dart';

void main() async {
  debugPrint('App Starting: ${DateTime.now()}');

  WidgetsFlutterBinding.ensureInitialized();

  // SplashScreen
  runApp(const SplashScreen());

  // Colocar as tarefas para carregar antes de iniciar o programa
  await Future.wait([]);

  // Teste de 3s
  await Future.delayed(
    const Duration(seconds: 3),
    () => debugPrint('Performance Monitoring Started: ${DateTime.now()}'),
  );

  runApp(ModularApp(module: AppModule(), child: const AppWidget()));

  debugPrint('App Loaded: ${DateTime.now()}');
}
